# MongoDB and Mongo Express Deployment

## Overview

This project focuses on deploying MongoDB and Mongo Express using Kubernetes. Below are the key steps and components involved in setting up the environment.

## Project Structure

### 1. MongoDB Deployment
- **Objective:** Set up a MongoDB instance.
- **Steps:**
  - Create MongoDB Deployment.
  - Establish a Secret for secure Mongo credentials.
  - Create an Internal Service for MongoDB.

### 2. Mongo Express Deployment
- **Objective:** Deploy Mongo Express for external access.
- **Steps:**
  - Create MongoExpress Deployment.
  - Set up a ConfigMap for the DB Server URL.
  - Create a Mongo Express External Service.

## Getting Started

1. **Prerequisites:**
   - Ensure you have a Kubernetes cluster running.
   - Have `kubectl` configured to interact with the cluster.

2. **Deploy MongoDB:**
   - Navigate to the `mongodb-deployment` directory.
   - Execute `kubectl apply -f mongodb-deployment.yaml` to create the MongoDB Deployment, Secret, and Internal Service.

3. **Deploy Mongo Express:**
   - Navigate to the `mongoexpress-deployment` directory.
   - Execute `kubectl apply -f mongoexpress-deployment.yaml` to set up the Mongo Express Deployment, ConfigMap, and External Service.

4. **Access Mongo Express:**
   - Get the external IP of the service: `kubectl get svc mongo-express-external-service`.
   - Open a web browser and navigate to `http://<external-ip>:8081` to access Mongo Express.

## Notes
- Ensure all sensitive information, especially credentials, is securely managed.
- Adjust configurations in YAML files as needed for your specific environment.

Feel free to reach out for any assistance or improvements!

**Happy Coding!**
